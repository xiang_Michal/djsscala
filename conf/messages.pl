
login.error.user.invalid=Invalid password or e-mail

registration.error.passwords.not.the.same=Wpisane hasla r�znia sie
registration.error.email.empty=Adres e-mail nie moze byc pusty.
registration.success=Rejestracja zakonczyla sie powodzeniem. Mozesz sie zalogowac.
registration.error.user.exists=Uzytkownik o podanym adresie e-mail juz istnieje.

form.unauthorized.error=Brak uprawnien do tego ekranu
form.registration.label=Rejestracja:
form.registration.button=Zarejestruj sie
form.login.label=Zaloguj sie:
form.login.button=Zaloguj sie
form.logut.button=Wyloguj sie
form.search.button=Szukaj
form.add.button=Dodaj
form.delete.reservation.button=Usun rezerwacje
show.result=Wynik wyszukiwania
travels.search=Gdzie chesz sie udac ?
travels.search.button=Wyszukaj
travels.search.text=Cel podr�zy
travels.all=Wszystkie wycieczki
travel.empty.price=Cena wycieczki musi byc uzupelniona
travel.startData.label=Data wyjazdu/wylotu
error=Brak wynik�w wyszukiwania, wyswietlenie calej listy
output.list.name=Miejsce docelowe
output.list.price=Cena
output.list.description=Opis
output.list.travel.by=Srodek transportu
form.reservation=Zarezerwoj bilet
from.cancel.reservation=Wycofaj rezerwacje
form.term=Termin wycieczki
form.travel.details=Szczegoly
form.reservation.list=Lista rezerwacji
form.reservation.date=Data rezerwacji
form.reservation.expire.date=Data rezerwacji
login.error.permissions=Zaloguj sie na konto
details.reservation.login.need=Aby dokonac rezerwacji musisz byc zalogowany.

registration.error.captcha=Bledny napis
form.edit.user.button=Edytuj swoje dane

output.place.reserved=Miejsce
travel.order.delete.success=Usinieto rezerwacje
common.button.check.if.available=Sprawdz dostepnosc
reservations.check.availability=Coc poszlo nie tak. Wprowadz dane jeszcze raz i spr�buj ponownie.

reservations.beggining.date.before.current=Wybrana data jest wczesniejsza niz dzisiejsza.
reservations.unknown.error=Wystapil blad podczas tworzenia rezerwacji. Sprawdz poprawnosc danych i spr�buj ponownie.
travel.order.make.success=Pomyslnie utworzona rezerwacja
registration.capcha=Wpisz poprawnie napis

update.error.haveReservation=Misisz usunac wszystkie rezerwacje aby m�c zmienic email
update.user.success=Pomyslnie zmienione dane
form.edit.user.label=Edycja danych uzytkownika
edit.error.userId=Nie mozna edytowac innego uzytkownika

form.updateUser.Button=Zmien dane