package controllers

import java.io.File

import controllers.TravelCtrl._
import controllers.UserCtrl._
import models.{UserPlay, Travel, DB,Reservation}
import org.joda.time.LocalDate
import play.api.mvc.{Action, Controller}

/**
 * Created by Dom on 2015-04-18.
 */
object ReservationCtrl extends Controller{

  def makeNewReservation(name:String)= Action {implicit request =>
    val travelO:Option[Travel] = DB.query[Travel].whereEqual("travelDest",name).fetchOne()
    val travel:Travel=travelO.get
    val email=request.session.get("USER")
    val reservation=models.Reservation(LocalDate.now,travel.startDate, email.get,travel,0)
    DB.save(reservation)
    Redirect("/")

  }

  def deleteReservation(name:String)=  Action { implicit request =>
    val travelO: Option[Travel] = DB.query[Travel].whereEqual("travelDest", name).fetchOne()
    val travel: Travel = travelO.get
    val email = request.session.get("USER")


    val reservation: Option[Reservation] = DB.query[Reservation].whereEqual("travel", travel).whereEqual("owner",email.get).fetchOne()
    DB.delete(reservation.get)
    Redirect("/user/box")
  }
}
