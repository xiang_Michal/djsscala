package controllers

import models.UserPlay
import models.Reservation
import tools.KeyCodes
import scala.None
import scala.util.{Failure, Try}
import play.api.mvc._
import models.DB
import play.api.mvc.Flash
import play.api.i18n.Messages
object UserCtrl extends Controller {


  def login = Action {implicit request: RequestHeader=>
    Ok(views.html.login.render(request.flash))
  }
  def registration() = Action{implicit request: RequestHeader=>
    Ok(views.html.registration.render(request.flash))
  }

  def newRegistration = TODO

  def box = Action {implicit request: RequestHeader=>
    val email=request.session.get("USER").get

    val reservations = DB.query[Reservation].whereEqual("owner",email).fetch().toList
    Ok(views.html.box.render(reservations,request.session))
  }

  def editUser(name:String) = Action {implicit request: RequestHeader=>
    Ok(views.html.login.render(request.flash))
  }

  def signIn = Action { implicit request =>

    val email: String = request.body.asFormUrlEncoded.get("email")(0)
    val password: String = request.body.asFormUrlEncoded.get("password")(0)

    val userPlayDb:Option[UserPlay] = DB.query[UserPlay].whereEqual("email", email).whereEqual("passwordHash", password).fetchOne()


    if (userPlayDb.isDefined) {
      if(userPlayDb.get.isAdmin().eq("99")){
        Redirect("/").withNewSession.addingToSession("USER" -> email, "ADMIN" -> userPlayDb.get.isAdmin())
      }else{
        Redirect("/").withNewSession.addingToSession("USER" -> email)
      }

    } else {
      Redirect("/user/login").flashing("ERROR" -> Messages("login.error.user.invalid"))
    }
  }

  def signOut = Action { request =>
    Redirect("/").withNewSession
  }

  def signUp= Action { implicit request =>

    val email: String = request.body.asFormUrlEncoded.get("email")(0)
    val password: String = request.body.asFormUrlEncoded.get("password")(0)
    val repeatedPassword: String = request.body.asFormUrlEncoded.get("repeatedPassword")(0)
    val firstName: String = request.body.asFormUrlEncoded.get("firstName")(0)
    val lastName: String = request.body.asFormUrlEncoded.get("lastName")(0)



    if (email == null || email.isEmpty) {
      Redirect("/user/registration").flashing("ERROR" -> Messages("registration.error.email.empty"))
    } else if (!password.equals(repeatedPassword)) {
      Redirect("/user/registration").flashing("ERROR" -> Messages("registration.error.passwords.not.the.same"))
    } else {
      val userPlay = models.UserPlay(email, firstName, lastName, password, "1")
      DB.save(userPlay)
      Redirect("/")
    }
  }


  def updateUser= TODO


}