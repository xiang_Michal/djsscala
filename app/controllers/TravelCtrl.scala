package controllers

import java.io.File

import controllers.UserCtrl._
import org.joda.time.LocalDate
import play.api.i18n.Messages
import play.api.mvc.{Action, Controller}
import models.{Travel, DB}
/**
 * Created by Dom on 2015-04-18.
 */
object TravelCtrl extends Controller with Secured{

  def getAll= TODO

  def getImage(path: String) = Action {implicit request =>
    Ok.sendFile(new File(path))
  }


  def list = Action {implicit request =>
    val travels = DB.query[Travel].fetch().toList
    Ok(views.html.travel.myTravel.render(travels, request.session))
  }

  def create=withAuth {username=> implicit request =>

    Ok(views.html.travel.create.render(request.session,request.flash))
  }

  def addNewTravel= Action(parse.multipartFormData){implicit request =>
    val formData = (request.body).asFormUrlEncoded
    val travelDest:String=formData.get("travelDest").get(0)
    val description:String=formData.get("description").get(0)
    val travelPrice:Double=(formData.get("travelPrice").get(0)).toDouble
    val startDate:String=formData.get("startDate").get(0)
    val capacity:Int=(formData.get("capacity").get(0)).toInt
    val travelBy:String=formData.get("travelBy").get(0)
    var filePath=""
    request.body.file("travelImage").map {
      picture =>
        val fileName = picture.filename
        val path="D:/ScalaImages/"
        filePath=path+fileName
        if (picture.filename.length > 0) {
          picture.ref.moveTo(new File(path+fileName))
        }
    }

    if (travelDest == null || travelDest.isEmpty() ){
      Redirect("/create").flashing("ERROR" -> Messages("reservations.check.availability"))
    } else if (travelPrice == null) {
      Redirect("/create").flashing("ERROR" -> Messages("travel.empty.price"))
    } else {
      val jodaTime:LocalDate= LocalDate.parse(startDate)
      val travel = models.Travel(travelDest, travelBy, travelPrice,filePath, description, capacity,jodaTime)
      DB.save(travel)
      Redirect("/").withSession()
    }
}

  def travelDetail(name:String)=Action {implicit request =>

    val travel:Option[Travel] = DB.query[Travel].whereEqual("travelDest",name).fetchOne()

    Ok(views.html.travel.details.render(travel.get, request.session))
  }

  def search =TODO

  def serachTab=TODO

  def getReservations(id:String)=TODO


}
