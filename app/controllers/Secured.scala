package controllers


/**
 * Created by Xiang on 2015-05-12.
 */

import play.api.i18n.Messages
import play.api.mvc.Security;
import play.api.mvc._
trait Secured {

  def username(request: RequestHeader) = request.session.get("ADMIN")

  def onUnauthorized(request: RequestHeader) = Results. Redirect("/user/login").flashing("ERROR" -> Messages("form.unauthorized.error"))

  def withAuth(f: => String => Request[AnyContent] => Result) = {
    Security.Authenticated(username, onUnauthorized) { user =>
      Action(request => f(user)(request))
    }
  }

}
