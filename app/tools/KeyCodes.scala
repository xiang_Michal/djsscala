package tools

/**
 * Created by Dom on 2015-04-19.
 */
final object KeyCodes {
  final val FLASH_ERROR: String = "ERROR"
  final val FLASH_SUCCESS: String = "SUCCESS"
  final val SESSION_KEY_USER: String = "USER"
  final val SESSION_ADMIN: String = "ADMIN"
  final val FLASH_CAPTCHA: String = "CAPTCHA"
}

