package models
/**
 * Created by Xiang on 2015-04-18.
 */

case class UserPlay(email:String,firstName:String,lastName:String,passwordHash:String,role:String) {

  def this(email: String, passwordHash: String) =
    this(email, "", "", passwordHash, "")

  def isAdmin(): String = {
    if (role.contains("99"))
      return "99";
    else
      return "";
  }
}


