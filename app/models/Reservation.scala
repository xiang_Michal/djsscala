package models

import org.joda.time.LocalDate


/**
 * Created by Xiang on 2014-11-11.
 */
case class Reservation (creationDate:LocalDate,expiryDate:LocalDate,owner:String,travel:Travel,state:Int)