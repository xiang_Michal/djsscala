package models


import models.UserPlay
import sorm._


/**
 * Created by Xiang on 2015-05-07.
 */


object DB extends Instance (
  entities = Set()
  +Entity[UserPlay](unique = Set() + Seq("email"))
  +Entity[Travel](unique = Set() + Seq("travelDest"))
  +Entity[Reservation](),
  url = "jdbc:postgresql://localhost:5432/postgres",
  user = "postgres",
  password = "admin",
  initMode = InitMode.Create,
  poolSize = 12
)
