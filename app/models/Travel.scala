package models


import org.joda.time.LocalDate

/**
 * Created by Xiang on 2014-11-11.
 */
case class Travel(travelDest: String, travelBy: String, travelPrice: Double, travelImage: String, description: String, capacity: Int, startDate: LocalDate) {



}